# Project1 Plant tracking

This is a simple jdbc web server that allows users to login and add and view plants to track from an azure postgres database. 

# Rooty!

## Available Paths
---
### ***register***
##### Register a new account
Simply type in a unique username and a password
### ***login***
Logging in allows you to start adding and managing your plants

### ***Manage plants***
You can add new plants, remove plants, and update your current plants
