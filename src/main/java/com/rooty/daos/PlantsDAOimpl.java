package com.rooty.daos;


import com.rooty.models.PlantItem;
import com.rooty.models.UserItem;
import com.rooty.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class PlantsDAOimpl implements PlantsDAO{

    @Override
    public void createPlant(PlantItem plantItem) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.save(plantItem);
            tx.commit();
        }
    }

    @Override
    public List<PlantItem> getAllPlants() {
        try (Session s = HibernateUtil.getSession()) {
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            return s.createQuery("from PlantItem", PlantItem.class).list();
        }
    }

    @Override
    public PlantItem getOnePlant(int id) {
        try(Session s = HibernateUtil.getSession()){
            PlantItem plantItem = s.get(PlantItem.class, id);
            return plantItem;
        }
    }


    @Override
    public void updatePlant(String id, PlantItem plantItem) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.update(plantItem);
            tx.commit();
        }
    }

    @Override
    public void deletePlant(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(getOnePlant(id));
            tx.commit();
        }
    }

    @Override
    public List<PlantItem> getAllPlantsByName(String plantName) {
        try(Session s = HibernateUtil.getSession()){
            Query<PlantItem> itemQuery = s.createQuery("from PlantItem where plantName = :plantName", PlantItem.class);
            itemQuery.setParameter("owner",plantName);
            return itemQuery.list();
        }
    }

    @Override
    public List<PlantItem> getAllPlantsByUsername(String plant_owner) {
        UsersDAO usersDAO = new UsersDAOimpl();
        try(Session s = HibernateUtil.getSession()){
            UserItem userItem = usersDAO.getOneUser(plant_owner);
            Query<PlantItem> itemQuery = s.createQuery("from PlantItem where owner = :plantOwner", PlantItem.class);
            itemQuery.setParameter("plantOwner",userItem);
            return itemQuery.list();
        }
    }
}
