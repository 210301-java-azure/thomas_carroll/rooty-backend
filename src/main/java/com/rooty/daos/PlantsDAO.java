package com.rooty.daos;

import com.rooty.models.PlantItem;

import java.util.List;

public interface PlantsDAO {
    public void createPlant(PlantItem plantItem);

    public List<PlantItem> getAllPlants();

    public PlantItem getOnePlant(int id);

    public void updatePlant(String id, PlantItem plantItem);

    public void deletePlant(int id);

    public List<PlantItem> getAllPlantsByName(String plantName);

    public List<PlantItem> getAllPlantsByUsername(String plant_owner);
}
