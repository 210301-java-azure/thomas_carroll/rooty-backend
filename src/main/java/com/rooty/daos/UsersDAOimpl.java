package com.rooty.daos;


import com.rooty.models.UserItem;
import com.rooty.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class UsersDAOimpl implements UsersDAO {
    @Override
    public void createUser(UserItem userItem) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.save(userItem);
            tx.commit();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<UserItem> getAllUsers() {
        try (Session s = HibernateUtil.getSession()) {
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            List<UserItem> x = s.createQuery("from UserItem", UserItem.class).list();
            System.out.println(x);
            return x;
        }
    }

    @Override
    public UserItem getOneUser(String id) {
        try (Session s = HibernateUtil.getSession()) {
            UserItem userItem = s.get(UserItem.class, id);
            return userItem;
        }
    }

    @Override
    public void updateUser(String id, UserItem userItem) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.update(id, userItem);
            tx.commit();
        }
    }

    @Override
    public void deleteUser(String id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(getOneUser(id));
            tx.commit();
        }

    }

    @Override
    public String getPasswordQuery(String username) {
        try (Session s = HibernateUtil.getSession()) {
            UserItem userItem = s.get(UserItem.class, username);
            return userItem.getPassword();
        }
    }
}