package com.rooty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="plant_table")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class PlantItem implements Serializable {

    @Id
    @Column(name = "plant_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int plantId;

    @Column(name = "plant_name")
    private String plantName;

    @ManyToOne
    private UserItem owner;

    @Column(name = "soil_used")
    private String soilUsed;

    @Column(name = "fertilizer_used")
    private String fertilizerUsed;

    @Column(name = "days_between_watering")
    private int daysBetweenWatering;

    @Column(name = "weeks_between_feeding")
    private int weeksBetweenFeeding;

    @Column(name = "user_caption")
    private String userCaption;

    @Column(name = "leafs_upvotes")
    private int leafsUpvotes;

    public PlantItem(int plantId, String plantName, UserItem owner, String fertilizerUsed, String soilUsed, int daysBetweenWatering, int weeksBetweenFeeding, String userCaption, int leafsUpvotes) {
        this.plantId = plantId;
        this.plantName = plantName;
        this.owner = owner;
        this.soilUsed = soilUsed;
        this.fertilizerUsed = fertilizerUsed;
        this.daysBetweenWatering = daysBetweenWatering;
        this.weeksBetweenFeeding = weeksBetweenFeeding;
        this.userCaption = userCaption;
        this.leafsUpvotes = leafsUpvotes;
    }

    public PlantItem(String plantName, String soilUsed, String fertilizerUsed, int daysBetweenWatering, int weeksBetweenFeeding, String userCaption){
        this.plantName = plantName;
        this.soilUsed = soilUsed;
        this.fertilizerUsed = fertilizerUsed;
        this.daysBetweenWatering = daysBetweenWatering;
        this.weeksBetweenFeeding = weeksBetweenFeeding;
        this.userCaption = userCaption;
    }
    public PlantItem(){
        super();
    }

    public PlantItem(int id){super();}




    public int getPlantId() {return plantId;}
    public void setPlantId(int plantId) {this.plantId = plantId;}

    public UserItem getOwner() {
        return owner;
    }

    public void setOwner(UserItem owner) {
        this.owner = owner;
    }

    public String getSoilUsed() {return soilUsed;}
    public void setSoilUsed(String soilUsed) {this.soilUsed = soilUsed;}
    public String getFertilizerUsed() {return fertilizerUsed;}
    public void setFertilizerUsed(String fertilizerUsed) {this.fertilizerUsed = fertilizerUsed;}
    public int getDaysBetweenWatering() {return daysBetweenWatering;}
    public void setDaysBetweenWatering(int daysBetweenWatering) {this.daysBetweenWatering = daysBetweenWatering;}
    public int getWeeksBetweenFeeding() {return weeksBetweenFeeding;}
    public void setWeeksBetweenFeeding(int weeksBetweenFeeding) {this.weeksBetweenFeeding = weeksBetweenFeeding;}
    public String getUserCaption() {return userCaption;}
    public void setUserCaption(String userCaption) {this.userCaption = userCaption;}
    public int getLeafsUpvotes() {return leafsUpvotes;}
    public void setLeafsUpvotes(int leafsUpvotes) {this.leafsUpvotes = leafsUpvotes;}
    public String getPlantName() {return plantName;}
    public void setPlantName(String plantName) {this.plantName = plantName;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlantItem plantItem = (PlantItem) o;
        return plantId == plantItem.plantId && daysBetweenWatering == plantItem.daysBetweenWatering && weeksBetweenFeeding == plantItem.weeksBetweenFeeding && Objects.equals(owner, plantItem.owner) && Objects.equals(soilUsed, plantItem.soilUsed) && Objects.equals(fertilizerUsed, plantItem.fertilizerUsed) && Objects.equals(userCaption, plantItem.userCaption);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plantId, owner, soilUsed, fertilizerUsed, daysBetweenWatering, weeksBetweenFeeding, userCaption);
    }

    @Override
    public String toString() {
        return "PlantItem{" +
                "plantId=" + plantId +
                ", owner=" + owner +
                ", soilUsed='" + soilUsed + '\'' +
                ", fertilizerUsed='" + fertilizerUsed + '\'' +
                ", daysBetweenWatering=" + daysBetweenWatering +
                ", weeksBetweenFeeding=" + weeksBetweenFeeding +
                ", userCaption='" + userCaption + '\'' +
                '}';
    }
}
