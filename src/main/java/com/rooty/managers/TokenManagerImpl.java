package com.rooty.managers;


import com.rooty.service.TokenService;
import com.rooty.service.TokenServiceImpl;
import io.javalin.http.ForbiddenResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class TokenManagerImpl implements TokenManager {
    TokenService tokenService = new TokenServiceImpl();
    private final Key key;

    public TokenManagerImpl() {
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    @Override
    public String issueToken(String userName, String password) {
        String token = Jwts.builder().setAudience(userName).setSubject(password).signWith(key).compact();
        return token;
    }
    @Override
    public boolean authorizeToken(String token) {
        try {
            String subjectPassword = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
            String subjectUser = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getAudience();
            if(tokenService.authorizeToken(subjectPassword, subjectUser)){
                return true;
            } else {
                return false;
            }

        } catch (Exception ex){
            throw new ForbiddenResponse();
        }
    }
}
