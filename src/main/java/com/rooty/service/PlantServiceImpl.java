package com.rooty.service;

import com.rooty.daos.PlantsDAO;
import com.rooty.daos.PlantsDAOimpl;
import com.rooty.models.PlantItem;

import java.util.List;

public class PlantServiceImpl implements PlantService {

    PlantsDAO plantsDAO = new PlantsDAOimpl();

    @Override
    public void createPlant(PlantItem plantItem) {
        plantsDAO.createPlant(plantItem);
    }

    @Override
    public List<PlantItem> getAllPlants() {
        return plantsDAO.getAllPlants();
    }

    @Override
    public PlantItem getOnePlant(int id) {
        return plantsDAO.getOnePlant(id);
    }

    @Override
    public void updatePlant(String id, PlantItem plantItem) {
        plantsDAO.updatePlant(id, plantItem);
    }

    @Override
    public void deletePlant(int id) {
        plantsDAO.deletePlant(id);
    }

    @Override
    public List<PlantItem> getAllPlantsName(String plantName) {
        return plantsDAO.getAllPlantsByName(plantName);
    }

    @Override
    public List<PlantItem> getAllPlantsByUsername(String plant_owner) {
        return plantsDAO.getAllPlantsByUsername(plant_owner);
    }

}
