package com.rooty.service;

import com.rooty.daos.UsersDAO;
import com.rooty.daos.UsersDAOimpl;
import com.rooty.models.UserItem;

public class TokenServiceImpl implements TokenService {
    UsersDAO usersDAO = new UsersDAOimpl();

    @Override
    public Boolean userLogin(UserItem userItem) {
        String username = userItem.getUsername();
        String password = userItem.getPassword();
        System.out.println("Was able to retrieve username: " + username + " and password: " + password);
        String dbPassword = usersDAO.getPasswordQuery(username);
        if (password.equals(dbPassword)) {
            System.out.println("Username and password worked");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean authorizeToken(String password, String username) {
        String dbPassword = usersDAO.getPasswordQuery(username);
        if (password.equals(dbPassword)) {
            return true;
        } else {
            return false;
        }
    }
}
