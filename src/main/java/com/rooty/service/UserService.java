package com.rooty.service;

import com.rooty.models.UserItem;

import java.util.List;
public interface UserService {
    public void createUser(UserItem user);
    public List<UserItem> getAllUsers();
    public UserItem getOneUser(String s);
    public void updateUser(String s, UserItem user);
    public void deleteUser(String s);
}
