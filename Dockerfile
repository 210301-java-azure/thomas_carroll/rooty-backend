FROM java:8
COPY build/libs/rooty-backend-1.1-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar rooty-backend-1.1-SNAPSHOT.jar
